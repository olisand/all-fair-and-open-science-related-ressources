# All FAIR and Open Science-related ressources

**_Attention : cette liste est pour l'instant qu'un ramassis de liens. Elle a besoin de curation et de commentaires, quand quelqu'on aura le temps. Mais il faut bien commencer quelque part...._**

**Guides**


- [Guide de bonnes pratiques sur la gestion des données de la recherche](https://mi-gt-donnees.pages.math.unistra.fr/guide/00-introduction.html)

**Sémantique**

- [Article: 39 Hints to Facilitate the Use of Semantics for Data on Agricult](https://datascience.codata.org/article/10.5334/dsj-2020-047/)

**Métadonnées etc**

- [https://academic.oup.com/gigascience/article/9/12/giaa144/6034785](Making experimental data tables in the life sciences more FAIR: a pragmatic approach)
- [https://inrae.github.io/ODAM/about/](https://inrae.github.io/ODAM/about/)
- [Retour d'expérience "Diffuser librement les métadonnées de la recherche avec Vivo (REX de l'EHESS et de l'UQAM)"](http://devlog.cnrs.fr/jdev2020/t5.p20210218)

**Science ouverte**


- [Étude de faisabilité d’un service générique d’accueil et de diffusion des données simples : premières études](https://www.ouvrirlascience.fr/etude-de-faisabilite-dun-service-generique-daccueil-et-de-diffusion-des-donnees-simples-premieres-etudes/)


**Solutions/outils**

- [bitwarden](https://bitwarden.com/)
- [Length Complexity for passwords](https://imgur.com/gallery/zFyBtyA)
- transfert de fichiers https://www.globus.org/

**Formation/training**

- [ELIXIR: Identifying gaps in current training](https://docs.google.com/forms/d/e/1FAIpQLSca-40-pxZIhgEL18bgqOnPYtsnn-7gNOq6d7QIwbFnUh7y-g/viewform)
- [FAIR data stewardship (for the life sciences) course](https://www.aanmelder.nl/fair-data-stewardship-2021)


**Arbres de décision ouverture des données**

- [arbre de décision du Cirad](https://coop-ist.cirad.fr/gerer-des-donnees/diffuser-les-donnees/testez-l-arbre-aide-a-la-decision-sur-la-diffusion-des-donnees-de-recherche)

**Entrepôts**

- [Aide au choix d’un entrepot](http://busec2.u-bordeaux.fr/aide-choix-entrepot/#général)

**Data stewardship**

- [Competency Hub](https://competency.ebi.ac.uk/framework/datasteward/1.0)

** Significant papers**

- [The FAIR Guiding Principles for scientific data management and stewardship](https://www.nature.com/articles/sdata201618),     Mark D. Wilkinson, Michel Dumontier _et al_, Barend Mons, Scientific Data, volume 3, (2016)
